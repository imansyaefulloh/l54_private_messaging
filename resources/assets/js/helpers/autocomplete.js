import algolia from 'algoliasearch'
import autocomplete from 'autocomplete.js'

var index = algolia('5EHF1M8J2Y', 'a0b2e094911d68dbbef5c3c533ff79ab')

export const userautocomplete = selector => {
    var users = index.initIndex('users')

    return autocomplete(selector, {}, {
        source: autocomplete.sources.hits(users, {hitsPerPage: 10}),
        displayKey: 'name',
        templates: {
            suggestion(suggestion) {
                return '<span>'+suggestion.name+'</span>'
            },
            empty: '<div class="aa-empty">No people found.</div>'
        }
    })
}
