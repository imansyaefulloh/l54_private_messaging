<?php

namespace App;

use Auth;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Conversation extends Model
{
    protected $dates = ['last_reply'];

    /**
     * RELATIONSHIPS
     */
    
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function users()
    {
        return $this->belongsToMany(User::class);
    }

    public function replies()
    {
        return $this->hasMany(Conversation::class, 'parent_id')->latestFirst();
    }

    public function parent()
    {
        return $this->belongsTo(Conversation::class, 'parent_id');
    }

    /**
     * QUERY SCOPES
     */
    
    public function scopeLatestFirst($query)
    {
        return $query->orderBy('created_at', 'desc');
    }

    /**
     * METHODS
     */

    public function usersExceptCurrentlyAuthenticated()
    {
        return $this->users()->where('user_id', '!=', Auth::user()->id);
    }

    public function touchLastReply()
    {
        $this->last_reply = Carbon::now();
        $this->save();
    }

    public function isReply()
    {
        return $this->parent_id !== null;
    }
    
}

















